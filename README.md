# Setting up GitLab Coverage Visualisation

## Issue: Coverage Visualisation Does Not Work

- [commit "add some tests"](https://gitlab.com/mvfwd/gitlab-coverage/-/commit/cbb69293de098a8be3ead47cf182031417322e72)
- Changes do not have coverage visualisation
- [Stackoverflow question](https://stackoverflow.com/questions/68520813/gitlab-test-coverage-visualization-is-not-working-on-a-simple-example)

![Commit with no coverage visualisation](./screenshot.png "Commit with no coverage visualisation")

## Links

- Reported GitLab [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/336861)
- [Test coverage visualization](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html)
  - v.12.10 [link](https://docs.gitlab.com/12.10/ee/user/project/merge_requests/test_coverage_visualization.html)
    - `:coverage_report_view`
- [Add test coverage results to a merge request](https://docs.gitlab.com/ee/ci/pipelines/settings.html#add-test-coverage-results-to-a-merge-request)
  - Meny > Projects
  - Settings > CI/CD
  - General pipelines
  - Test coverage parsing
  - pytest-cov (Python). Example: `^TOTAL.+?(\d+\%)$`
- Badges <img src="https://gitlab.com/AviaTorq/gitlab-coverage/badges/master/coverage.svg"/>

  - Settings > General > Badges
  - Link: (TBD)
  - Image: https://gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg
